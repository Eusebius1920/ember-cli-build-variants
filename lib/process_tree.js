'use strict';
var path = require( 'path' );

var bTreeMerger = require( 'broccoli-merge-trees' );
var bFunnel     = require( 'broccoli-funnel' );


module.exports = function( tree, flavor ){
  var original = new bFunnel(tree, {
    exclude: [
      /\.[A-Z]{3,}(\.[A-Za-z]*)?/
    ]
  });
  var funnel = null;
  if( flavor ) {
    funnel = new bFunnel(tree, {
      include: [
        '**/*.' + flavor.name.toUpperCase() + '.*',
        '**/*.' + flavor.name.toUpperCase()
      ],
      getDestinationPath: function(relativePath) {
        let fileName = path.basename( relativePath );
        fileName = fileName.replace(
          '.'+ flavor.name.toUpperCase(),
          ''
        );

        const newPath = path.posix.join(
          path.dirname(relativePath),
          fileName
        );
        return newPath;
      }
    });
  }

  if( flavor )
    return new bTreeMerger(
      [original, funnel], {
        overwrite: true,
        annotation: 'Flavoring ' + flavor.name + '.'
      }
    );
  else
    return original;
}
