# Changelog

## master
* Nothing yet.

## 1.0.4
* Added postprocessTree hook. This means also files inside `/public` are processed.

## 1.0.3
* Updated build image to ember(-cli) 2.18.0
* Fixed a bug with paths on windows (Thanks @ChristophNiehoff)

## 1.0.2
* Made slight changes to README.md docs.
* Update to ember(-cli) 2.18.0

## 1.0.1
* Added repository link to package.json.

## 1.0.0
* Initial release.
