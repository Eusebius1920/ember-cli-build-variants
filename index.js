'use strict';

var treeProcessor = require( './lib/process_tree' );
var flavorLoader  = require( './lib/load_flavor' );

var flavor = null;
var flavorLoaded = false;

module.exports = {
  name: 'ember-cli-build-variants',
  config( env, baseConfig ){
    if(!flavorLoaded){
      flavor = flavorLoader( this.project );
      flavorLoaded = true;
    }
    baseConfig.flavor = flavor || { name: null, data: {}};
  },
  preprocessTree( type, tree ){
    return treeProcessor( tree, flavor );
  },
  postprocessTree( type, tree ){
    return treeProcessor( tree, flavor );
  }
};
