import { helper } from '@ember/component/helper';
import config from 'ember-get-config';

export function flavorData(/*params, hash*/) {
  return config.flavor.data;
}

export default helper(flavorData);
